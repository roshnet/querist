from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
from flask import session
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash as hashgen
from werkzeug import check_password_hash as chk
# CUSTOM MODULES
from extras.greetings import greeting
from utils.validate import validate
from db_config import db

app = Flask(__name__)
app.config['SECRET_KEY'] = 'i4d1fr15s8a14'
app.config['MYSQL_DATABASE_HOST'] = db['host']
app.config['MYSQL_DATABASE_USER'] = db['user']
app.config['MYSQL_DATABASE_PASSWORD'] = db['password']
app.config['MYSQL_DATABASE_DB'] = db['name']

mysql = MySQL()
mysql.init_app(app)

conn = mysql.connect()
cursor = conn.cursor()


@app.route('/')
def index():
    return redirect(url_for('home'))


@app.route('/home')
def home():
    # Forcefully log out if logged in.
    session['curr_uid'] = None
    session['logged_in'] = False
    session['user_type'] = None
    return render_template('index.html', greeting=greeting, title='Home')



# ALL LOGIN ROUTES

@app.route('/login', methods=['GET'])
def login():
    # Automatically opens up the required page.
    # Goes to relogin-route if the user is not logged in.
    # If logged in, opens the required welcome page(based on session['user_type']).
    # NOTE: relogin-route automatically opens the required login page.

    if not session['logged_in'] == True:
        return render_template('login_choice.html', title='Log In')
    elif session['user_type'] == 'student':
        return render_template('student_home.html', title='Welcome')
    elif session['user_type'] == 'teacher':
        return render_template('teacher_home.html', title='Welcome')
    else:
        # In case anything goes wrong with strings..
        return redirect(url_for('home'))


@app.route('/students/login', methods=['GET', 'POST'])
def login_student():
    # Login page is rendered when method == GET;
    # Form data is processed when method == POST;

    if session['logged_in'] == True:
        return redirect(url_for('welcome'))

    if request.method == 'POST':
        username = request.form['username']
        passwd = request.form['passwd']

        # Add an `if` to abort query execution if non-allowed chars present,
        # thus preventing SQLIA.

        sql = "SELECT username,password FROM students WHERE username='{}';"
        cursor.execute(sql.format(username))
        match = cursor.fetchone()
        if match is not None:
            _passwd = match[1]  # hashed pswd..
            if not chk(_passwd, passwd):
                return render_template('login_student.html',
                                        title='Log In',
                                        errmsg='Invalid credentials. Please try again.',
                                        persist_uname=username)
            else:
                # using else so that user does NOT login
                # if by any slightest chance `match` != None..
                # On login success:
                session['curr_uid'] = username
                session['logged_in'] = True
                session['user_type'] = 'student'
                return redirect(url_for('student_home'))
        elif match is None:
            return render_template('login_student.html',
                                    title='Log In',
                                    errmsg='Invalid credentials. Please try again.',
                                    persist_uname=username)
    if request.method == 'GET':
        return render_template('login_student.html',
                                title='Log In')


@app.route('/teachers/login', methods=['GET', 'POST'])
def login_teacher():

    if session['logged_in'] == True:
        return redirect(url_for('welcome'))

    if request.method == 'POST':
        username = request.form['username']
        passwd = request.form['passwd']

        # Add an `if` to abort query execution if non-allowed chars present,
        # thus preventing SQLIA.

        sql = "SELECT username,password FROM teachers WHERE username='{}';"
        cursor.execute(sql.format(username))
        match = cursor.fetchone()
        if match is not None:
            _passwd = match[1]  # hashed pswd..
            if not chk(_passwd, passwd):
                return render_template('login_teacher.html',
                                        title='Log In',
                                        errmsg='Invalid credentials. Please try again.',
                                        persist_uname=username)
            else:
                # using else so that user does NOT login
                # if by any slightest chance `match` != None..
                # On login success:
                session['curr_uid'] = username
                session['logged_in'] = True
                session['user_type'] = 'student'
                return redirect(url_for('teacher_home'))
        elif match is None:
            return render_template('login_student.html',
                                    title='Log In',
                                    errmsg='Invalid credentials. Please try again.',
                                    persist_uname=username)
    if request.method == 'GET':
        return render_template('login_teacher.html',
                                title='Log In')



# ALL SIGNUP ROUTES

@app.route('/signup')
def signup():
    # Shows up the signup user type page, if the user is logged out.
    if session['logged_in'] == True:
        return redirect(url_for('welcome'))
    # By default:
    return render_template('signup_choice.html', title='Create an Account')


@app.route('/students/signup', methods=['GET', 'POST'])
def signup_student():
    # Signup page is rendered when method == GET;
    # Form data is processed when method == POST;
    # Signup page with flashed message(s) is rendered if input is invalid.

    if session['logged_in'] == True:
        return redirect(url_for('welcome'))

    if request.method == 'POST':
        username = request.form['username']
        passwd = request.form['passwd']
        name = request.form['name']
        email = request.form['email']
        institute = request.form['institute']
        valid = validate(name, username, passwd)
        if valid == 1:
            # Checking if username already exists (taken)..
            sql = "SELECT s_id FROM students WHERE username='{}';"
            cursor.execute(sql.format(username))
            match = cursor.fetchone()
            if match is None:
                sql = '''
                INSERT INTO students (username,password,email,name,institute)
                VALUES (%s,%s,%s,%s,%s);
                '''
                try:
                    cursor.execute(sql, (username,
                                         hashgen(passwd, method='sha1'),
                                         email,
                                         name,
                                         institute))
                    conn.commit()
                except Exception as e:
                    errmsg = '''
                    Something weird happened. We will soon fix it.<br/>
                    Click <a href="{{url_for('signup_student')">here</a> to retry.
                    '''
                    errmsg += '\n{}'.format(e)
                    # Comment above line in production mode.
                    return errmsg

                session['curr_uid'] = username
                session['logged_in'] = True
                session['user_type'] = 'student'
                return redirect(url_for('welcome',
                                        usr=username))
            else:
                # When username already exists (taken)..
                return render_template('signup_student.html',
                                        title='Sign Up',
                                        errmsg='Username unavailable')
        else:
            return render_template('signup_student.html',
                                    title='Sign Up',
                                    errmsg='Invalid fields!')
    if request.method == 'GET':
        return render_template('signup_student.html', title='Create Account')


@app.route('/teachers/signup', methods=['GET', 'POST'])
def signup_teacher():

    if session['logged_in'] == True:
        return redirect(url_for('welcome'))

    if request.method == 'POST':
        name = request.form['name']
        username = request.form['username']
        passwd = request.form['passwd']
        email = request.form['email']
        institute = request.form['institute']
        valid = validate(name, username, passwd)
        if valid == 1:
            # Checking if username exists or not..
            sql = "SELECT t_id FROM teachers WHERE username='{}';"
            cursor.execute(sql.format(username))
            match = cursor.fetchone()
            if match is None:
                sql = '''
                INSERT INTO teachers (username,password,email,name,institute)
                VALUES (%s,%s,%s,%s,%s);
                '''
                try:
                    cursor.execute(sql, (username,
                                         hashgen(passwd, method='sha1'),
                                         email,
                                         name,
                                         institute))
                    conn.commit()
                except Exception as e:
                    errmsg = '''
                    Something weird happened. We will soon fix it.<br/>
                    Click <a href="{{url_for('signup_teacher'}})">here</a> to retry.
                    '''
                    errmsg += '\nError: \n{}'.format(e)
                    # Comment above line in production mode.
                    return errmsg

                session['curr_uid'] = username
                session['logged_in'] = True
                session['user_type'] = 'teacher'
                return redirect(url_for('teacher_home'))
            else:
                # When username is taken..
                return render_template('signup_teacher.html',
                                        title='Sign Up',
                                        errmsg='Username unavailable')
        else:
            return render_template('signup_teacher.html',
                                    title='Sign Up',
                                    errmsg='Invalid fields!')
    if request.method == 'GET':
        return render_template('signup_teacher.html', title='Create an Account')



# ALL MEMBER ROUTES

@app.route('/welcome')
def welcome():
    if session['logged_in'] == True:
        if session['user_type'] == 'student':
            return redirect(url_for('student_home'))
        if session['user_type'] == 'teacher':
            return redirect(url_for('teacher_home'))
    else:
        return redirect(url_for('relogin'))


@app.route('/students/home')
def student_home():
    if session['logged_in'] == True and session['user_type'] == 'student':
        return render_template('student_home.html',
                                title='Welcome',
                                username=session['curr_uid'])
    return redirect(url_for('relogin'))


@app.route('/teachers/home')
def teacher_home():
    if session['logged_in'] == True and session['user_type'] == 'teacher':
        return render_template('teacher_home.html',
                                title='Welcome',
                                username=session['curr_uid'])
    return redirect(url_for('relogin'))



# ALL STANDARD (reuse) ROUTES

@app.route('/relogin')
def relogin():
    # A standard route to point to, when a user is not logged in,
    # but attempts to open a URL which is allowed only for a 
    # logged-in user.
    # NOTE: The route opens the required login page, based on the
    # session key 'user_type'.

    errmsg = 'You must log in first!'
    # errmsg = 'You must log in to continue.'
    if session['logged_in'] == True:
        if session['user_type'] == 'student':
            return render_template('login_student.html',
                                    title='Log In',
                                    errmsg=errmsg)
        if session['user_type'] == 'teacher':
            return render_template('login_teacher.html',
                                    title='Log In',
                                    errmsg=errmsg) 
    else:
        # When logged in:
        return redirect(url_for('login'))


@app.route('/logout')
def logout():
    # Reset all user configuration variables.
    session['curr_uid'] = None
    session['user_type'] = None
    session['logged_in'] = False
    # Start afresh..
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)
